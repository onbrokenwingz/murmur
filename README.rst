Mumble Server Container
=======================

Docker
------

Check this project out on dockerhub_ or ``docker pull jwhet/murmur``.
More information on getting started with docker here_.

.. _dockerhub: https://hub.docker.com/r/jwhet/murmur/
.. _here: https://docs.docker.com/engine/getstarted/step_one/#/step-1-get-docker


Building the Container
----------------------
After cloning the repo:

::

    docker build -t ${dockerhub}/${image-name} .

Example:

::

    docker build -t jwhet/murmur .

Running the Container
---------------------
The following will run (and download) the container with the
name "voice" and the default ports mapped:

::

    # Starting
    docker run -d -p 64738:64738 --name voice jwhet/murmur
    # Stopping
    docker stop voice

Using Compose
-------------
Optionally, you can use `docker-compose` while in the reop
to run the container and have all options set automatically.

::

    # Starting
    docker-compose up -d
    # Stoping
    docker-compose down

SuperUser Password
------------------
**SuperUser** password is set on the first run of the container
and can be read from the logs (use `docker logs voice`). Change the
**SuperUser** password:

::

    docker exec voice murmurd -supw test

Contributing
------------
1. Clone the repo.
2. Create a new branch.
3. Submit a merge request.

